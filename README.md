# My Azure DevOps Repos

## Expense Tracker (ASP.NET Core & Blazor)
[![Build Status](https://dev.azure.com/teemutynkkynen/Expense%20Tracker/_apis/build/status/Expense%20Tracker?branchName=master)](https://dev.azure.com/teemutynkkynen/Expense%20Tracker/_build/latest?definitionId=10&branchName=master)<br><br>
https://dev.azure.com/teemutynkkynen/Expense%20Tracker

## Web Project (Angular & ASP.NET Core)
[![Build Status](https://dev.azure.com/teemutynkkynen/Web%20Project/_apis/build/status/Web%20Project?branchName=master)](https://dev.azure.com/teemutynkkynen/Web%20Project/_build/latest?definitionId=7&branchName=master)<br><br>
https://dev.azure.com/teemutynkkynen/Web%20Project

## Simple Currency Converter (WinForms)
https://dev.azure.com/teemutynkkynen/Currency%20Converter

## Transport Renting Service (WPF)
[![Build Status](https://dev.azure.com/teemutynkkynen/Transport%20Renting%20Service/_apis/build/status/Transport%20Renting%20Service?branchName=master)](https://dev.azure.com/teemutynkkynen/Transport%20Renting%20Service/_build/latest?definitionId=4&branchName=master)<br><br>
https://dev.azure.com/teemutynkkynen/Transport%20Renting%20Service

## Person Manager (WinForms)
https://dev.azure.com/teemutynkkynen/Person%20Manager